# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-06-01 08:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('findAnimalApp', '0010_auto_20160601_1144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='picture',
            field=models.ImageField(default=None, upload_to='images'),
        ),
    ]
