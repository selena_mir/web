# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-29 16:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('findAnimalApp', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Notice_category',
            new_name='NoticeCategory',
        ),
    ]
