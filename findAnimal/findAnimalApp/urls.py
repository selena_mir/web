from django.conf.urls import url

from . import views

app_name = 'findAnimalApp'
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^(?P<notice_id>[0-9]+)/$',views.notice,name="notice"),
    url(r'^new_notice/$',views.create_notice,name="create_notice"),
   #url(r'^edit_notice/(?P<notice_id>[0-9]+)/$',views.edit_notice,name="edit_notice"),
]