from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.core.files.uploadedfile import SimpleUploadedFile

from .models import Comment, Change, NoticeCategory, Breed, Animal, Location, Notice, Photo
from .forms import NoticeForm,CommentForm,PhotoForm


# Create your views here.
def index(request):
    notices_list = Notice.objects.order_by('title')
    context = {
        'notices_list': notices_list,
    }
    #output = ' '.join([n.title for n in notices_list])
    return render(request,'findAnimalApp/index.html',context)


def notice(request,notice_id):
    notice_obj = get_object_or_404(Notice, pk=notice_id)
    photo = Photo.objects.filter(id_notice=notice_obj)[:1]
    if photo:
        photo = photo.get()
    else:
        photo = None
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            newComment = form.save(commit=False)
            newComment.id_notice = notice_obj
            newComment.save()
    comments = Comment.objects.filter(id_notice=notice_obj)
    commentForm = CommentForm()
    context={
        'notice': notice_obj,
        'comments': comments,
        'addcomment': commentForm,
        'photo': photo
    }
    return render(request,"findAnimalApp/notice.html",context)

def create_notice(request):
    if request.method == 'POST':
        form = NoticeForm(request.POST)
        photo = PhotoForm(request.POST,request.FILES)
        if form.is_valid():
            new_notice = form.save()
        if photo.is_valid():
            photo_i = photo.save(commit=False)
            photo_i.id_notice = new_notice
            photo_i.save()
        return HttpResponseRedirect('/findAnimalApp/'+new_notice.id.__str__())
    elif 'notice_id' in request.GET:
            notice_id = request.GET['notice_id']
            notice = get_object_or_404(Notice, pk=notice_id)
            form = NoticeForm(instance=notice)
            photo = Photo.objects.filter(id_notice = notice)[:1]
            if photo:
                photo = PhotoForm(instance=photo.get())
            else:
                photo = PhotoForm()
    else:
        form = NoticeForm()
        photo = PhotoForm()
    return render(request, "findAnimalApp/create_notice.html", {'form': form, 'photo': photo})

# def edit_notice(request,notice_id):
#     notice = get_object_or_404(Notice, pk=notice_id)
#     form = NoticeForm(request.POST,notice)