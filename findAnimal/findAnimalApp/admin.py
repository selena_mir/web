from django.contrib import admin

# Register your models here.
from .models import Comment, Change, Notice, NoticeCategory,Location,Animal,Breed,Photo

admin.site.register(Comment)
admin.site.register(Change)
admin.site.register(Notice)
admin.site.register(NoticeCategory)
admin.site.register(Location)
admin.site.register(Animal)
admin.site.register(Breed)
admin.site.register(Photo)