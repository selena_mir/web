from _ast import mod

from django.db import models


# Create your models here.
class NoticeCategory(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Location (models.Model):
    name = models.CharField(max_length=50)
    parent = models.ForeignKey('self',on_delete=models.CASCADE,null=True,default=None,blank=True)

    def __str__(self):
        result = self.name
        if self.parent:
            result += ', ' + self.parent.__str__()
        return result


class Animal (models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Breed (models.Model):
    name = models.CharField(max_length=50)
    id_animal = models.ForeignKey(Animal,on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Notice (models.Model):
    title = models.CharField(max_length=50)
    text = models.CharField(max_length=200)
    id_animal = models.ForeignKey(Animal,on_delete=models.CASCADE)
    id_location = models.ForeignKey(Location,on_delete=models.CASCADE)
    id_breed = models.ForeignKey(Breed,on_delete=models.CASCADE)
    id_category = models.ForeignKey(NoticeCategory,on_delete=models.CASCADE)
    owner_email = models.EmailField(max_length=50)

    def __str__(self):
        return self.title


class Photo (models.Model):
    id_notice = models.ForeignKey(Notice,on_delete=models.CASCADE)
    picture = models.ImageField(default=None,upload_to="images")

    def get_url(self):
        return "findAnimalApp/" + self.picture.url


class Comment (models.Model):
    id_notice = models.ForeignKey(Notice,on_delete=models.CASCADE)
    author = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    text = models.CharField(max_length=200)

    def __str__(self):
        return self.author + ': ' + self.text


class Change (models.Model):
    id_notice = models.ForeignKey(Notice,on_delete=models.CASCADE)
    activity = models.DateTimeField()
    key = models.CharField(max_length=10)
