from django import forms
from django.forms import ModelForm, Textarea
from .models import Notice, Comment, Photo

class NoticeForm(ModelForm):
    class Meta:
        model = Notice
        fields = ['id_category', 'title', 'text', 'id_animal', 'id_breed', 'id_location','owner_email']
        widgets = {
            'text': Textarea(attrs={'cols': 80, 'rows': 10}),
        }
        labels = {
            'title':('Заголовок'),
            'text':('Текст'),
            'id_animal':('Животное'),
            'id_category':('Категория'),
            'id_breed':('Порода'),
            'id_location':('Место'),
            'owner_email':('Email владельца'),
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['author', 'text', 'email']
        widgets = {
            'text': Textarea(attrs={'cols': 80, 'rows': 5}),
            'id_notice': forms.HiddenInput(),
        }
        labels = {
            'author': 'Автор',
            'text': 'Текст',
            'email': 'E-mail'
        }

class PhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = ['picture']
        labels = {
            'picture': 'Фото',
        }