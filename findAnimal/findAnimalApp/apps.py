from django.apps import AppConfig


class FindanimalappConfig(AppConfig):
    name = 'findAnimalApp'
